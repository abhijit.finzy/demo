import {suite, test} from "mocha-typescript";
import {BookController} from "../../src/controller/book.controller";
import {BookService} from "../../src/services/book.service";
import {instance, mock, verify, when} from "ts-mockito";
import {assert, expect} from "chai";
import {Book} from "../../src/models/Book";
import {BookStatus} from "../../src/models/BookStatus";

@suite
export class BookControllerSpec {

    @test
    shouldGetAllBooks(): void {
        const bookService = mock(BookService);
        const bookServiceInstance = instance(bookService);
        const bookController = new BookController(bookServiceInstance);
        const expectedBooks = [new Book(1, "Prime", "Rhonda Bryne", 2007),
            new Book(2, "A Book of Simple Living", "Rhonda Bryne", 2007)];
        when(bookService.getAllBooks()).thenReturn(expectedBooks);

        const actualBooks = bookController.getAllBooks();

        expect(actualBooks).to.be.deep.equals(expectedBooks);
    }

    @test
    shouldCheckoutBook(): void {
        const bookService = mock(BookService);
        const bookServiceInstance = instance(bookService);
        const bookController = new BookController(bookServiceInstance);

        bookController.checkoutBook(1);

        verify(bookService.checkOutBook(1)).once();
    }

    @test
    shouldThrowErrorForInvalidBookId(): void {
        const bookService = mock(BookService);
        const bookServiceInstance = instance(bookService);
        const bookController = new BookController(bookServiceInstance);

        assert.throws(() => bookController.checkoutBook(null), Error, "Book Id is Invalid")
    }

    @test
    shouldThrowErrorForNegativeBookId(): void {
        const bookService = mock(BookService);
        const bookServiceInstance = instance(bookService);
        const bookController = new BookController(bookServiceInstance);

        assert.throws(() => bookController.checkoutBook(-1), Error, "Book Id is Invalid")
    }

    @test
    shouldGetBooksByStatusAvailable(): void {
        const bookService = mock(BookService);
        const bookServiceInstance = instance(bookService);
        const bookController = new BookController(bookServiceInstance);
        const expectedBooks = [new Book(1, "Prime", "Rhonda Bryne", 2007),
            new Book(2, "A Book of Simple Living", "Rhonda Bryne", 2007)];
        when(bookService.getBooksByStatus(BookStatus.Available)).thenReturn(expectedBooks);

        const actualBooks = bookController.getBooksByStatus(BookStatus.Available);

        expect(actualBooks).to.be.deep.equals(expectedBooks);
    }

    @test
    shouldReturnABook(): void {
        const bookService = mock(BookService);
        const bookServiceInstance = instance(bookService);
        const bookController = new BookController(bookServiceInstance);

        bookController.returnBook(2);

        verify(bookService.returnBook(2)).once();
    }

    @test
    shouldThrowErrorForInvalidBookIdOnReturnBook(): void {
        const bookService = mock(BookService);
        const bookServiceInstance = instance(bookService);
        const bookController = new BookController(bookServiceInstance);

        assert.throws(() => bookController.returnBook(null), Error, "Book Id is Invalid")
    }

    @test
    shouldThrowErrorForNegativeBookIdOnReturnBook(): void {
        const bookService = mock(BookService);
        const bookServiceInstance = instance(bookService);
        const bookController = new BookController(bookServiceInstance);

        assert.throws(() => bookController.returnBook(-1), Error, "Book Id is Invalid")
    }
}
