import {suite, test} from "mocha-typescript";
import {instance, mock, verify, when} from "ts-mockito";
import {MovieService} from "../../src/services/movie.service";
import {MovieController} from "../../src/controller/movie.controller";
import {Movie} from "../../src/models/Movie";
import {MovieStatus} from "../../src/models/MoviesStatus";
import {assert, expect} from "chai";


@suite
export class MovieControllerSpec {
    private movieController: MovieController;
    private movieService: MovieService;

    before() {
        this.movieService = mock(MovieService);
        const movieServiceInstance = instance(this.movieService);
        this.movieController = new MovieController(movieServiceInstance);
    }

    @test

    shouldGetAllMovies(): void {
        const movies = [new Movie(1, "Sonar Kella", 1990, "Satyajit Ray", 10, MovieStatus.Available),
            new Movie(2, "Pother Panchali", 1980, "Satyajit Ray", 10, MovieStatus.Available)];
        when(this.movieService.getAllMovie()).thenReturn(movies);

        const allMovies = this.movieController.getAllMovies();

        expect(allMovies).to.deep.equal(movies);
    }

    @test
    shouldCheckoutMovie(): void {
        this.movieController.checkoutMovie(1);

        verify(this.movieService.checkoutMovie(1)).once();
    }

    @test
    shouldThrowErrorForInvalidBookId(): void {

        assert.throws(() => this.movieController.checkoutMovie(null), Error, "Movie Id is Invalid")
    }

    @test
    shouldThrowErrorForNegativeBookId(): void {

        assert.throws(() => this.movieController.checkoutMovie(-1), Error, "Movie Id is Invalid")
    }

}