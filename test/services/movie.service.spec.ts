import {suite, test} from "mocha-typescript";
import {capture, deepEqual, instance, mock, when} from "ts-mockito";
import {MovieService} from "../../src/services/movie.service";
import {MovieRepository} from "../../src/repositories/movie.repository";
import {Movie} from "../../src/models/Movie";
import {MovieStatus} from "../../src/models/MoviesStatus";
import {assert, expect} from "chai";
import {EntityNotFound} from "../../src/Error/EntityNotFound";
import {EntityNotAvailable} from "../../src/Error/EntityNotAvailable";

@suite
export class MovieServiceSpec {
    private movieService: MovieService;
    private movieRepository: MovieRepository;

    before() {
        this.movieRepository = mock(MovieRepository);
        this.movieService = new MovieService(instance(this.movieRepository));
    }


    @test
    shouldGetAllMovie() {
        const movies = [new Movie(1, "Sonar Kella", 1990, "Satyajit Ray", 10, MovieStatus.Available),
            new Movie(2, "Pother Panchali", 1980, "Satyajit Ray", 10, MovieStatus.Available)];
        when(this.movieRepository.getMoviesByStatus(MovieStatus.Available)).thenReturn(movies);

        const actualMovies = this.movieService.getAllMovie();

        expect(actualMovies).to.deep.equal(movies);
    }

    @test
    shouldCheckoutMovie() {
        const id = 1;
        const movie = new Movie(1, "Sonar Kella", 1990, "Satyajit Ray", 10, MovieStatus.Available);
        const expectedMovie = new Movie(1, "Sonar Kella", 1990, "Satyajit Ray", 10, MovieStatus.Checkout);
        when(this.movieRepository.getMovie(id)).thenReturn(movie);

        this.movieService.checkoutMovie(expectedMovie._id);
        when(this.movieRepository.updateMovie(deepEqual(expectedMovie))).thenReturn(expectedMovie);

        const [actualMovie] = capture(this.movieRepository.updateMovie).last();
        expect(actualMovie).to.deep.equal(expectedMovie);
    }

    @test
    public shouldThrowEntityNotFoundErrorWhenMovieIsNotFoundOnCheckoutAMovie(): void {
        const id = 0;
        when(this.movieRepository.getMovie(id)).thenReturn(null);

        assert.throws(() => this.movieService.checkoutMovie(id), EntityNotFound, "Movie Not Found");

    }

    @test
    public shouldThrowEntityNotAvailableErrorWhenMovieIsNotInAvailableStateOnCheckoutAMovie(): void {
        const movie = new Movie(1, "Sonar Kella", 1990, "Satyajit Ray", 10, MovieStatus.Checkout);

        when(this.movieRepository.getMovie(1)).thenReturn(movie);

        assert.throws(() => this.movieService.checkoutMovie(1), EntityNotAvailable, "Movie Not Available");

    }

}