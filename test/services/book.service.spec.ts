import {suite, test} from "mocha-typescript";
import {BookService} from "../../src/services/book.service";
import {assert, expect} from 'chai';
import {BookRepository} from "../../src/repositories/book.repository";
import {capture, deepEqual, instance, mock, verify, when} from "ts-mockito";
import {Book} from "../../src/models/Book";
import {BookStatus} from "../../src/models/BookStatus";
import {EntityNotFound} from "../../src/Error/EntityNotFound";
import {EntityNotAvailable} from "../../src/Error/EntityNotAvailable";
import {EntityAlreadyAvailable} from "../../src/Error/EntityAlreadyAvailable";

@suite
class BookServiceSpec {
    private bookService: BookService;
    private bookRepository: BookRepository;

    public before() {
        this.bookRepository = mock(BookRepository);
        this.bookService = new BookService(instance(this.bookRepository));
    }

    @test
    public shouldGetAllBooks(): void {
        const expectedBooks = [new Book(1, "Prime", "Rhonda Bryne", 2007),
            new Book(2, "A Book of Simple Living", "Rhonda Bryne", 2007)];
        when(this.bookRepository.getAllBooks()).thenReturn(expectedBooks);

        const actualBooks = this.bookService.getAllBooks();

        expect(actualBooks).to.be.deep.equal(expectedBooks);
    }

    @test
    public shouldCheckoutABook(): void {
        const book = new Book(1, "The Secret", "Rhonda Bryne", 2007, BookStatus.Available);
        const expectedBook = new Book(1, "The Secret", "Rhonda Bryne", 2007, BookStatus.Checkout);
        when(this.bookRepository.getBook(1)).thenReturn(book);

        this.bookService.checkOutBook(book._id);
        when(this.bookRepository.updateBook(deepEqual(expectedBook))).thenReturn(expectedBook);

        const [actualBook] = capture(this.bookRepository.updateBook).last();
        expect(actualBook).to.deep.equal(expectedBook);
    }

    @test
    public shouldThrowEntityNotFoundErrorWhenBookIsNotFoundOnCheckoutABook(): void {
        const id = 0;
        when(this.bookRepository.getBook(id)).thenReturn(null);

        assert.throws(() => this.bookService.checkOutBook(id), EntityNotFound, "Book Not Found");

    }

    @test
    public shouldThrowEntityNotAvailableErrorWhenBookIsNotInAvailableStateOnCheckoutABook(): void {
        const book = new Book(1, "The Secret", "Rhonda Bryne", 2007, BookStatus.Checkout);

        when(this.bookRepository.getBook(1)).thenReturn(book);

        assert.throws(() => this.bookService.checkOutBook(1), EntityNotAvailable, "Book Not Available");

    }

    @test
    public shouldGetAvailableBooks(): void {
        const books = [new Book(1, "The Secret", "Rhonda Bryne", 2007, BookStatus.Available),
            new Book(2, "Five Point Someone", "Chetan Bhagat", 2007, BookStatus.Available)];
        when(this.bookRepository.getBooksByStatus(BookStatus.Available)).thenReturn(books);

        const availableBooks = this.bookService.getBooksByStatus("Available");

        expect(books).to.deep.equal(availableBooks);
        expect(books).to.be.of.length(2);
    }

    @test
    public shouldReturnABook(): void {
        const id = 1;
        const book = new Book(id, "The Secret", "Rhonda Bryne", 2007, BookStatus.Checkout);
        const expectedBook = new Book(id, "The Secret", "Rhonda Bryne", 2007, BookStatus.Available);
        when(this.bookRepository.getBook(id)).thenReturn(book);

        this.bookService.returnBook(id);
        when(this.bookRepository.updateBook(deepEqual(expectedBook))).thenReturn(expectedBook);

        verify(this.bookRepository.updateBook(deepEqual(expectedBook))).once();
        const [actualBook] = capture(this.bookRepository.updateBook).last();
        expect(actualBook).to.deep.equal(expectedBook);
    }

    @test
    public shouldThrowEntityNotFoundErrorWhenBookIsNotFoundOnReturnBook(): void {
        const id = 0;
        when(this.bookRepository.getBook(id)).thenReturn(null);

        assert.throws(() => this.bookService.returnBook(id), EntityNotFound, "Book Not Found");

    }

    @test
    public shouldThrowEntityAlreadyAvailableErrorWhenBookIsNotInAvailableStateOnReturnBook(): void {
        const book = new Book(1, "The Secret", "Rhonda Bryne", 2007, BookStatus.Available);

        when(this.bookRepository.getBook(1)).thenReturn(book);

        assert.throws(() => this.bookService.returnBook(1), EntityAlreadyAvailable, "Book Is Already Returned");

    }

}