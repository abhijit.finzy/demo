import {suite, test} from "mocha-typescript";
import {Movie} from "../../src/models/Movie";
import {MovieStatus} from "../../src/models/MoviesStatus";
import {expect} from "chai";

@suite
export class MovieSpec {

    @test
    shouldReturnTrueIfStatusIsAvailable() {
        const movie = new Movie(1, "Sonar Kella", 1990, "Satyajit Ray", 10, MovieStatus.Available);

        expect(movie.isAvailable()).to.be.true;
    }

    @test
    public shouldCheckout(): void {
        const movie = new Movie(1, "Sonar Kella", 1990, "Satyajit Ray", 10, MovieStatus.Available);

        movie.checkout();

        expect(movie.isCheckedout()).to.be.true;
    }

    @test
    public shoudCheckIfBookIsCheckedout(): void {
        const availableMovie = new Movie(1, "Sonar Kella", 1990, "Satyajit Ray", 10, MovieStatus.Available);
        const checkedoutMovie = new Movie(1, "Sonar Kella", 1990, "Satyajit Ray", 10, MovieStatus.Checkout);

        expect(availableMovie.isCheckedout()).to.be.false;
        expect(checkedoutMovie.isCheckedout()).to.be.true;
    }
}