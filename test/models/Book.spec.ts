import {suite, test} from "mocha-typescript";
import {Book} from "../../src/models/Book";
import {expect} from "chai";
import {BookStatus} from "../../src/models/BookStatus";

@suite
export  class BookSpec {

    @test
    public shouldCheckout():void{
        const availableBook = new Book(1,"The Secret","Rhonda Bryne" , 2007,BookStatus.Available);
        availableBook.checkout();

        expect(availableBook.isCheckedout()).to.be.true;
    }

    c;

    @test
    public shouldReturn():void{
        const availableBook = new Book(1,"The Secret","Rhonda Bryne" , 2007,BookStatus.Checkout);
        availableBook.return();

        expect(availableBook.isAvailable()).to.be.true;
    }


}
