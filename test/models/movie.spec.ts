import {suite, test} from "mocha-typescript";
import {Movie} from "../../src/models/Movie";
import {MovieStatus} from "../../src/models/MoviesStatus";
import {expect} from "chai";

@suite
export class MovieSpec{

    @test
    shouldReturnTrueIfStatusIsAvailable(){
        const movie = new Movie(1,"Sonar Kella", 1990,"Satyajit Ray",10,MovieStatus.Available);

        expect(movie.isAvailable()).to.be.true;
    }

}