import {suite, test} from "mocha-typescript";
import {IdGenerator} from "../../src/utils/id-generator";
import {expect} from "chai";

@suite
class IdGeneratorSpec {
    @test
    shouldCreateNewId(): void {
        const idGenerator = new IdGenerator();
        const beforeTime = Date.now();

        const actualId = idGenerator.generateId();

        expect(actualId).to.gte(beforeTime);
        expect(actualId).to.lte(Date.now());
    }
}