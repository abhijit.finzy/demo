import {suite, test} from "mocha-typescript";
import {BookRepository} from "../../src/repositories/book.repository";
import {expect} from "chai";
import {Book} from "../../src/models/Book";
import {BookStatus} from "../../src/models/BookStatus";

@suite
export class BookRepositorySpec {
    @test
    public shouldGetTwoBooks(): void {
        const bookRepository = new BookRepository();

        const twoBooks = bookRepository.getAllBooks();

        expect(twoBooks).to.be.of.length(2);
    }

    @test
    public shouldGetFivePointSomeoneAndTheSecrets(): void {
        const bookRepository = new BookRepository();
        const expectedFirstBook = new Book(1, "The Secret","Rhonda Bryne" , 2007);
        const expectedSecondBook = new Book(2, "Five Point Someone","Chetan Bhagat" , 2007);

        const twoBooks = bookRepository.getAllBooks();

        expect(twoBooks).to.deep.include(expectedFirstBook);
        expect(twoBooks).to.deep.include(expectedSecondBook);
    }

    @test
    public shouldGetBook():void {
        const bookRepository = new BookRepository;
        const expectedBook = new Book(1, "The Secret","Rhonda Bryne" , 2007);

        const returnedBook = bookRepository.getBook(1);

        expect(returnedBook).to.deep.equal(expectedBook)
    }

    @test
    public getBooksByStatus():void{
        const bookRepository = new BookRepository;
        const expectedBooks = [new Book(2, "Five Point Someone","Chetan Bhagat" , 2007, BookStatus.Available),
            new Book(1, "The Secret","Rhonda Bryne" , 2007, BookStatus.Available)];

        const actualBooks = bookRepository.getBooksByStatus(BookStatus.Available);

        expect(actualBooks).to.deep.equal(expectedBooks);
        expect(actualBooks).to.be.length(2);
    }

    @test
    public shouldUpdateBook(): void {
        const actualBook = new Book(1, "The Secret","Rhonda Bryne" , 2007, BookStatus.Checkout);
        const bookRepository = new BookRepository();

        const updateBook = bookRepository.updateBook(actualBook);

        const allBooks = bookRepository.getAllBooks();
        expect(actualBook).to.deep.include(updateBook);
        expect(allBooks).to.deep.include(updateBook);
        expect(allBooks).to.be.length(2);
    }

}