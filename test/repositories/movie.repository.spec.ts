import {suite, test} from "mocha-typescript";
import {MovieRepository} from "../../src/repositories/movie.repository";
import {MovieStatus} from "../../src/models/MoviesStatus";
import {Movie} from "../../src/models/Movie";
import {expect} from "chai";

@suite
export class MovieRepositorySpec {

    @test
    shouldGetMovieByStatusAvailable(): void {
        const movieRepository = new MovieRepository();
        const movies = [new Movie(1, "Sonar Kella", 1990, "Satyajit Ray", 10, MovieStatus.Available),
            new Movie(2, "Pother Panchali", 1980, "Satyajit Ray", 10, MovieStatus.Available)];

        const actualMovies = movieRepository.getMoviesByStatus(MovieStatus.Available);

        expect(actualMovies).to.deep.equal(movies);
    }

    @test
    shouldUpdateMovie(): void {
        const movie = new Movie(1, "Sonar Kella", 1990, "Satyajit Ray", 10, MovieStatus.Checkout);
        const movieRepository = new MovieRepository();

        const actualMovie = movieRepository.updateMovie(movie);

        expect(actualMovie).to.deep.equal(movie);
    }

    @test
    shouldGetMovie(): void {
        const movieRepository = new MovieRepository();
        const expectedMovie = new Movie(1, "Sonar Kella", 1990, "Satyajit Ray", 10, MovieStatus.Checkout);


        const actualMovie = movieRepository.getMovie(1);

        expect(actualMovie).to.deep.equal(expectedMovie);
    }
}