import {Book} from "../models/Book";
import {BookRepository} from "../repositories/book.repository";
import {EntityNotFound} from "../Error/EntityNotFound";
import {EntityNotAvailable} from "../Error/EntityNotAvailable";
import {BookStatus} from "../models/BookStatus";
import {EntityAlreadyAvailable} from "../Error/EntityAlreadyAvailable";

export class BookService {
    private bookRepository: BookRepository;

    constructor(bookRepository: BookRepository) {
        this.bookRepository = bookRepository;
    }

    public getAllBooks(): Book[] {
        return this.bookRepository.getAllBooks();
    }

    public checkOutBook(id: number): void {
        const book = this.bookRepository.getBook(id);
        if (book == null) {
            throw new EntityNotFound("Book Not Found");
        } else if (book.isCheckedout()) {
            throw new EntityNotAvailable("Book Not Available");
        } else {
            book.checkout();
            this.bookRepository.updateBook(book);
        }
    }

    public getBooksByStatus(status: string) {
        status = status.toUpperCase();
        switch (status) {
            case "AVAILABLE":
                return this.bookRepository.getBooksByStatus(BookStatus.Available);
        }

    }

    public returnBook(id: number): void {
        const book = this.bookRepository.getBook(id);
        if (book == null) {
            throw new EntityNotFound("Book Not Found");
        } else if (book.isAvailable()) {
            throw new EntityAlreadyAvailable("Book Is Already Returned");
        } else {
            book.return();
            this.bookRepository.updateBook(book);
        }
    }
}