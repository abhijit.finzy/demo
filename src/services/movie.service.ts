import {MovieRepository} from "../repositories/movie.repository";
import {Movie} from "../models/Movie";
import {MovieStatus} from "../models/MoviesStatus";
import {EntityNotFound} from "../Error/EntityNotFound";
import {EntityNotAvailable} from "../Error/EntityNotAvailable";

export class MovieService {
    private movieRepository: MovieRepository;

    constructor(movieRepository: MovieRepository) {
        this.movieRepository = movieRepository;
    }


    public getAllMovie(): Movie[] {
        return this.movieRepository.getMoviesByStatus(MovieStatus.Available);
    }

    public checkoutMovie(id: number): void {
        const movie = this.movieRepository.getMovie(id);
        if (movie == null) {
            throw new EntityNotFound("Movie Not Found");
        } else if (movie.isCheckedout()) {
            throw new EntityNotAvailable("Movie Not Available");
        } else {
            movie.checkout();
            this.movieRepository.updateMovie(movie);
        }
    }
}