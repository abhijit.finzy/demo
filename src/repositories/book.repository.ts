import {Book} from "../models/Book";
import {BookStatus} from "../models/BookStatus";

export class BookRepository {

    private static bookDB: Book[] = [new Book(2, "Five Point Someone","Chetan Bhagat" , 2007), new Book(1, "The Secret","Rhonda Bryne" , 2007)];

    public getAllBooks(): Book[] {
        return BookRepository.bookDB;
    }

    public updateBook(book: Book): Book {
        const originalBook = this.getBook(book._id);
        const index = BookRepository.bookDB.indexOf(originalBook);
        BookRepository.bookDB[index] = book;
        return BookRepository.bookDB[index];
    }

    public getBook(id: number): Book {
        return BookRepository.bookDB.find((book) => book._id === id);
    }

    public getBooksByStatus(status: BookStatus):Book[] {
        switch (status) {
            case BookStatus.Available:
                return BookRepository.bookDB.filter((book) => {return book.isAvailable()});
        }

    }
}