import {Movie} from "../models/Movie";
import {MovieStatus} from "../models/MoviesStatus";

export class MovieRepository {
    private static movieDB: Movie[] = [new Movie(1, "Sonar Kella", 1990, "Satyajit Ray", 10, MovieStatus.Available),
        new Movie(2, "Pother Panchali", 1980, "Satyajit Ray", 10, MovieStatus.Available)];


    public getMoviesByStatus(status: MovieStatus): Movie[] {
        switch (status) {
            case MovieStatus.Available:
                return MovieRepository.movieDB.filter((movie) => {
                    return movie.isAvailable()
                });
        }
    }

    public updateMovie(movie: Movie): Movie {
        const originalMovie = this.getMovie(movie._id);
        const index = MovieRepository.movieDB.indexOf(originalMovie);
        MovieRepository.movieDB[index] = movie;
        return MovieRepository.movieDB[index];
    }

    public getMovie(id: number): Movie {
        return MovieRepository.movieDB.find((movie) => movie._id === id);
    }
}