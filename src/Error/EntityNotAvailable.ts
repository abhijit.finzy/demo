export class EntityNotAvailable extends Error {
    constructor(message: string) {
        super(message);
    }
}
