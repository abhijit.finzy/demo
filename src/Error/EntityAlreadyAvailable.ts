export class EntityAlreadyAvailable extends Error {
    constructor(message: string) {
        super(message);
    }
}
