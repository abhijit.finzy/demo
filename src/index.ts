import express = require('express');
import {BookController} from "./controller/book.controller";
import {BookService} from "./services/book.service";
import {BookRepository} from "./repositories/book.repository";
import {EntityNotFound} from "./Error/EntityNotFound";
import {EntityNotAvailable} from "./Error/EntityNotAvailable";
import {MovieController} from "./controller/movie.controller";
import {MovieService} from "./services/movie.service";
import {MovieRepository} from "./repositories/movie.repository";

const app = express();

app.listen(3000, () => console.log('Server Is Up'));

app.get('/books', (req, res) => {
    if (req.query.status) {
        return res.send(new BookController(new BookService(new BookRepository())).getBooksByStatus(req.query.status));
    } else {
        const allBooks = new BookController(new BookService(new BookRepository())).getAllBooks();
        res.send(allBooks);
    }
});

app.patch('/book/:bookId/checkout', (req, res) => {
    const bookId = parseInt(req.params.bookId);
    try {
        new BookController(new BookService(new BookRepository())).checkoutBook(bookId);
    } catch (e) {
        if (e instanceof EntityNotFound) {
            return res.status(404).send(e.message);
        } else if (e instanceof EntityNotAvailable) {
            return res.status(404).send(e.message);
        } else if (e instanceof Error) {
            return res.status(404).send(e.message);
        }
    }
    return res.status(200).send(`Checkedout`);
});

app.patch('/book/:bookId/return', (req, res) => {
    const bookId = parseInt(req.params.bookId);
    try {
        new BookController(new BookService(new BookRepository())).returnBook(bookId);
    } catch (e) {
        if (e instanceof EntityNotFound) {
            return res.status(404).send(e.message);
        } else if (e instanceof EntityNotAvailable) {
            return res.status(404).send(e.message);
        } else if (e instanceof Error) {
            return res.status(404).send(e.message);
        }
    }
    return res.status(200).send(`Returned`);
});

app.get('/movies', (req, res) => {
    const allMovies = new MovieController(new MovieService(new MovieRepository())).getAllMovies();
    res.send(allMovies);
});

app.patch('/movie/:movieId/checkout', (req, res) => {
    const movieId = parseInt(req.params.movieId);
    try {
        new MovieController(new MovieService(new MovieRepository())).checkoutMovie(movieId);
    } catch (e) {
        if (e instanceof EntityNotFound) {
            return res.status(404).send(e.message);
        } else if (e instanceof EntityNotAvailable) {
            return res.status(404).send(e.message);
        } else if (e instanceof Error) {
            return res.status(404).send(e.message);
        }
    }
    return res.status(200).send(`Checkedout`);
});

