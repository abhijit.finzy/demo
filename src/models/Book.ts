import {BookStatus} from "./BookStatus";

export class Book {
    public readonly _id: number;
    private readonly name: string;
    private status : BookStatus;
    private readonly author: string;
    private readonly publishedYear: number;

    constructor(id: number, name: string, authorsName: string , publishedYear: number , status: BookStatus = BookStatus.Available) {
        this._id = id;
        this.name = name;
        this.status = status;
        this.author = authorsName;
        this.publishedYear = publishedYear;
    }

    public checkout(){
        this.status = BookStatus.Checkout;
    }

    public isCheckedout() : boolean{
        if (this.status == BookStatus.Checkout){
            return true;
        }else{

            return false;
        }
    }
    public isAvailable() : boolean{
        if (this.status == BookStatus.Available){
            return true;
        }else{

            return false;
        }
    }

    public return() {
        this.status = BookStatus.Available;
    }
}