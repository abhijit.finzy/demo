import {MovieStatus} from "./MoviesStatus";

export type  RATING_RANGE = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10;

export class Movie {
    public readonly _id: number;
    private readonly name: string;
    private readonly year: number;
    private readonly director: string;
    private readonly rating: RATING_RANGE;
    private status: MovieStatus;

    constructor(id: number, name: string, year: number, director: string, rating: RATING_RANGE, status: MovieStatus = MovieStatus.Available) {
        this._id = id;
        this.name = name;
        this.year = year;
        this.director = director;
        this.rating = rating;
        this.status = status;
    }

    public isAvailable(): boolean{
        if(this.status==MovieStatus.Available){
            return true;
        }
        return false;
    }

    public checkout(): void {
        this.status = MovieStatus.Checkout;
    }

    public isCheckedout(): boolean {
        if (this.status == MovieStatus.Checkout) {
            return true;
        }
        return false;
    }

}