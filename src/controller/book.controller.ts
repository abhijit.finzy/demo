import {Book} from "../models/Book";
import {BookService} from "../services/book.service";

export class BookController {

    private bookServiceInstance: BookService;

    constructor(bookService: BookService) {
        this.bookServiceInstance = bookService;
    }

    public getAllBooks(): Book[] {
        return this.bookServiceInstance.getAllBooks();
    }

    public checkoutBook(id: number) {
        if (!id || id < 0) {
            throw new Error("Book Id is Invalid");
        }
        this.bookServiceInstance.checkOutBook(id);
    }

    public getBooksByStatus(status: string): Book[] {
        return this.bookServiceInstance.getBooksByStatus(status);
    }

    public returnBook(id: number) {
        if (!id || id < 0) {
            throw new Error("Book Id is Invalid");
        }
        this.bookServiceInstance.returnBook(id);
    }
}
