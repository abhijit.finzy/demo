import {MovieService} from "../services/movie.service";
import {Movie} from "../models/Movie";

export class MovieController {

    private movieServiceInstance: MovieService;

    constructor(movieService: MovieService) {
        this.movieServiceInstance = movieService;
    }

    public getAllMovies(): Movie[] {
        return this.movieServiceInstance.getAllMovie();
    }

    public checkoutMovie(id: number) {
        if (!id || id < 0) {
            throw new Error("Movie Id is Invalid");
        }
        this.movieServiceInstance.checkoutMovie(id);
    }
}